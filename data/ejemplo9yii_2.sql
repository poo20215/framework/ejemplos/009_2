-- MySQL dump 10.13  Distrib 5.7.36, for Win64 (x86_64)
--
-- Host: localhost    Database: ejemplo9yii_2
-- ------------------------------------------------------
-- Server version	5.7.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `camioneros`
--

DROP TABLE IF EXISTS `camioneros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camioneros` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `salario` float(10,2) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `telefono` char(9) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camioneros`
--

LOCK TABLES `camioneros` WRITE;
/*!40000 ALTER TABLE `camioneros` DISABLE KEYS */;
INSERT INTO `camioneros` VALUES ('76492875H','Jose Luis',1450.00,'Santander','942674528','Calle Castilla 45'),('77954328J','Fernando',1245.50,'Barcelona','698075603','La Rambla 15'),('78956950U','Manuel',1380.00,'Madrid','649687321','Gran Via 13');
/*!40000 ALTER TABLE `camioneros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camiones`
--

DROP TABLE IF EXISTS `camiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camiones` (
  `matricula` char(7) NOT NULL,
  `modelo` varchar(50) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `potencia` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camiones`
--

LOCK TABLES `camiones` WRITE;
/*!40000 ALTER TABLE `camiones` DISABLE KEYS */;
INSERT INTO `camiones` VALUES ('2645TKP','Renault Trucks Gama T','Medianos','512CV'),('5874LHG','Volvo Gama FH','Medianos','500CV'),('6842TGQ','DAF Gama FT','Pesados','580CV'),('8968LPS','MAN Serie TGX','Megapesados','600CV'),('9006GIR','Mercedes Gama Actros','Ligeros','450CV');
/*!40000 ALTER TABLE `camiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conducir`
--

DROP TABLE IF EXISTS `conducir`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conducir` (
  `camiones_matricula` char(7) NOT NULL,
  `camioneros_dni` char(9) NOT NULL,
  PRIMARY KEY (`camiones_matricula`,`camioneros_dni`),
  KEY `fk_conducir_camioneros` (`camioneros_dni`),
  CONSTRAINT `fk_conducir_camioneros` FOREIGN KEY (`camioneros_dni`) REFERENCES `camioneros` (`dni`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_conducir_camiones` FOREIGN KEY (`camiones_matricula`) REFERENCES `camiones` (`matricula`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conducir`
--

LOCK TABLES `conducir` WRITE;
/*!40000 ALTER TABLE `conducir` DISABLE KEYS */;
INSERT INTO `conducir` VALUES ('6842TGQ','76492875H'),('9006GIR','76492875H'),('2645TKP','77954328J'),('6842TGQ','77954328J'),('5874LHG','78956950U'),('8968LPS','78956950U');
/*!40000 ALTER TABLE `conducir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribuir`
--

DROP TABLE IF EXISTS `distribuir`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribuir` (
  `camioneros_dni` char(9) NOT NULL,
  `paquetes_codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`camioneros_dni`,`paquetes_codigo`),
  UNIQUE KEY `paquetes_codigo` (`paquetes_codigo`),
  CONSTRAINT `fk_distribuir_camioneros` FOREIGN KEY (`camioneros_dni`) REFERENCES `camioneros` (`dni`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_distribuir_paquetes` FOREIGN KEY (`paquetes_codigo`) REFERENCES `paquetes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribuir`
--

LOCK TABLES `distribuir` WRITE;
/*!40000 ALTER TABLE `distribuir` DISABLE KEYS */;
INSERT INTO `distribuir` VALUES ('76492875H','57436'),('77954328J','63893'),('78956950U','67542'),('78956950U','74836'),('76492875H','78563'),('78956950U','84756'),('76492875H','85429'),('77954328J','85947'),('78956950U','89562'),('77954328J','89566');
/*!40000 ALTER TABLE `distribuir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `llegar`
--

DROP TABLE IF EXISTS `llegar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `llegar` (
  `paquetes_codigo` varchar(10) NOT NULL,
  `provincias_codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`paquetes_codigo`,`provincias_codigo`),
  UNIQUE KEY `paquetes_codigo` (`paquetes_codigo`),
  KEY `fk_llegar_provincias` (`provincias_codigo`),
  CONSTRAINT `fk_llegar_paquetes` FOREIGN KEY (`paquetes_codigo`) REFERENCES `paquetes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_llegar_provincias` FOREIGN KEY (`provincias_codigo`) REFERENCES `provincias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `llegar`
--

LOCK TABLES `llegar` WRITE;
/*!40000 ALTER TABLE `llegar` DISABLE KEYS */;
INSERT INTO `llegar` VALUES ('57436','2'),('63893','3'),('67542','1'),('74836','2'),('78563','1'),('84756','2'),('85429','2'),('85947','1'),('89562','3'),('89566','3');
/*!40000 ALTER TABLE `llegar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paquetes`
--

DROP TABLE IF EXISTS `paquetes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paquetes` (
  `codigo` varchar(10) NOT NULL,
  `descripcion` varchar(70) DEFAULT NULL,
  `destinatario` varchar(70) DEFAULT NULL,
  `direccion_destinatario` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paquetes`
--

LOCK TABLES `paquetes` WRITE;
/*!40000 ALTER TABLE `paquetes` DISABLE KEYS */;
INSERT INTO `paquetes` VALUES ('57436','Camiseta adidas','David','Calle El Monte 13, Santander'),('63893','pijama niño','Lucia','Avenida Castilla 13, Gijon'),('67542','Guitarra','Alberto','Gran Vía 63, Madrid'),('74836','Televisión','Patricia','Calle Burgos 23, Santander'),('78563','Mochila','Antonia','Calle Espiriu Santo 11, Madrid'),('84756','Libro','Arturo','Avenida de Bilbao, Torrelavega'),('85429','Ordenador','Alvaro','General Davila 50, Santander'),('85947','Balon de futbol','Jose Ramón','Calle La Castellana 46, Madrid'),('89562','Mochila','Antonia','Avd. Conde de Guadalhorce, Aviles'),('89566','Reloj','María','Plaza de la Constitucion 21, Oviedo');
/*!40000 ALTER TABLE `paquetes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincias` (
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES ('1','Madrid'),('2','Cantabria'),('3','Asturias');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-03 18:57:32
