<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Conducir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conducir-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'camiones_matricula')->dropDownList($model->getCamiones()) ?>

    <?= $form->field($model, 'camioneros_dni')->dropDownList($model->getCamioneros()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
