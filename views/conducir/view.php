<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Conducir */

$this->title = $model->camiones_matricula;
$this->params['breadcrumbs'][] = ['label' => 'Conducirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="conducir-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'camiones_matricula' => $model->camiones_matricula, 'camioneros_dni' => $model->camioneros_dni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'camiones_matricula' => $model->camiones_matricula, 'camioneros_dni' => $model->camioneros_dni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'camiones_matricula',
            'camioneros_dni',
        ],
    ]) ?>

</div>
