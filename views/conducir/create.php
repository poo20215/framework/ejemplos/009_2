<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conducir */

$this->title = 'Create Conducir';
$this->params['breadcrumbs'][] = ['label' => 'Conducirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conducir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
