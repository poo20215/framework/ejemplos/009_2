<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conducir */

$this->title = 'Update Conducir: ' . $model->camiones_matricula;
$this->params['breadcrumbs'][] = ['label' => 'Conducirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camiones_matricula, 'url' => ['view', 'camiones_matricula' => $model->camiones_matricula, 'camioneros_dni' => $model->camioneros_dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conducir-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
