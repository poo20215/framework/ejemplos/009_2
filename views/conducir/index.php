<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conducir';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conducir-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Conducir', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'camiones_matricula',
            'camionesMatricula.modelo',
            'camioneros_dni',
            'camionerosDni.nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
