<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <h1 style="text-align: center;">EMPRESA DE REPARTO</h1>
    
    <?= Html::img('@web/imgs/reparto_mercancias.jpg',['alt' => 'Diseño','class' => 'img-fluid']); ?>
</div>