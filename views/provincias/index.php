<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Provincias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provincias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Provincias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo',
            'nombre',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',  //orden de los botones    
                'buttons' => [
                'view' => function($url,$model){
                return Html::a('<i class="fas fa-eye"></i>',['provincias/view','codigo' => $model->codigo]);
                },
                'update' => function($url,$model){
                    return Html::a('<i class="fas fa-pencil-alt"></i>',['provincias/update','codigo' => $model->codigo]);
                    },
                'delete' => function($url,$model){
                    return Html::a('<i class="fas fa-trash-alt"></i>',['provincias/delete','codigo' => $model->codigo],
                            ['data' => ['confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post']]);
                    },           
                            
            ]],
        ],
    ]); ?>


</div>
