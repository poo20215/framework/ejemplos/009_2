<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Camioneros */

$this->title = 'Update Camioneros: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Camioneros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="camioneros-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
