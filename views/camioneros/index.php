<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Camioneros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camioneros-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Camioneros', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dni',
            'nombre',
            'salario',
            'poblacion',
            'telefono',
            //'direccion',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',  //orden de los botones    
                'buttons' => [
                'view' => function($url,$model){
                return Html::a('<i class="fas fa-eye"></i>',['camioneros/view','dni' => $model->dni]);
                },
                'update' => function($url,$model){
                    return Html::a('<i class="fas fa-pencil-alt"></i>',['camioneros/update','dni' => $model->dni]);
                    },
                'delete' => function($url,$model){
                    return Html::a('<i class="fas fa-trash-alt"></i>',['camioneros/delete','dni' => $model->dni],
                            ['data' => ['confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post']]);
                            },
                            ]
                ],
        ],
    ]); ?>


</div>
