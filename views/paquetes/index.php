<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paquetes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Paquetes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo',
            'descripcion',
            'destinatario',
            'direccion_destinatario',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',  //orden de los botones    
                'buttons' => [
                'view' => function($url,$model){
                return Html::a('<i class="fas fa-eye"></i>',['paquetes/view','codigo' => $model->codigo]);
                },
                'update' => function($url,$model){
                    return Html::a('<i class="fas fa-pencil-alt"></i>',['paquetes/update','codigo' => $model->codigo]);
                    },
                'delete' => function($url,$model){
                    return Html::a('<i class="fas fa-trash-alt"></i>',['paquetes/delete','codigo' => $model->codigo],
                            ['data' => ['confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post']]);
                    },           
                            
            ]],
        ],
    ]); ?>


</div>
