<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Camiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Camiones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'matricula',
            'modelo',
            'tipo',
            'potencia',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',  //orden de los botones    
                'buttons' => [
                'view' => function($url,$model){
                return Html::a('<i class="fas fa-eye"></i>',['camiones/view','matricula' => $model->matricula]);
                },
                'update' => function($url,$model){
                    return Html::a('<i class="fas fa-pencil-alt"></i>',['camiones/update','matricula' => $model->matricula]);
                    },
                'delete' => function($url,$model){
                    return Html::a('<i class="fas fa-trash-alt"></i>',['camiones/delete','matricula' => $model->matricula],
                            ['data' => ['confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post']]);
                    },           
                            
            ]],
        ],
    ]); ?>


</div>
