<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Llegar */

$this->title = 'Update Llegar: ' . $model->paquetes_codigo;
$this->params['breadcrumbs'][] = ['label' => 'Llegars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->paquetes_codigo, 'url' => ['view', 'paquetes_codigo' => $model->paquetes_codigo, 'provincias_codigo' => $model->provincias_codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="llegar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
