<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Llegar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="llegar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Llegar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'paquetes_codigo',
            'paquetesCodigo.descripcion',
            'provincias_codigo',
            'provinciasCodigo.nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
