<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuir */

$this->title = 'Update Distribuir: ' . $model->camioneros_dni;
$this->params['breadcrumbs'][] = ['label' => 'Distribuirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camioneros_dni, 'url' => ['view', 'camioneros_dni' => $model->camioneros_dni, 'paquetes_codigo' => $model->paquetes_codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribuir-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
