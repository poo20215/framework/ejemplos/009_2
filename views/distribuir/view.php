<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuir */

$this->title = $model->camioneros_dni;
$this->params['breadcrumbs'][] = ['label' => 'Distribuirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="distribuir-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'camioneros_dni' => $model->camioneros_dni, 'paquetes_codigo' => $model->paquetes_codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'camioneros_dni' => $model->camioneros_dni, 'paquetes_codigo' => $model->paquetes_codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'camioneros_dni',
            'paquetes_codigo',
        ],
    ]) ?>

</div>
