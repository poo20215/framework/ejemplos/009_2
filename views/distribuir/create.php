<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuir */

$this->title = 'Create Distribuir';
$this->params['breadcrumbs'][] = ['label' => 'Distribuirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
