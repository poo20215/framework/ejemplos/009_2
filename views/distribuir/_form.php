<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribuir-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'camioneros_dni')->dropDownList($model->getCamioneros()) ?>

    <?= $form->field($model, 'paquetes_codigo')->dropDownList($model->getPaquetes()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
