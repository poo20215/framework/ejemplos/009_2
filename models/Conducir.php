<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conducir".
 *
 * @property string $camiones_matricula
 * @property string $camioneros_dni
 *
 * @property Camioneros $camionerosDni
 * @property Camiones $camionesMatricula
 */
class Conducir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conducir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['camiones_matricula', 'camioneros_dni'], 'required'],
            [['camiones_matricula'], 'string', 'max' => 7],
            [['camioneros_dni'], 'string', 'max' => 9],
            [['camiones_matricula', 'camioneros_dni'], 'unique', 'targetAttribute' => ['camiones_matricula', 'camioneros_dni']],
            [['camioneros_dni'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['camioneros_dni' => 'dni']],
            [['camiones_matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Camiones::className(), 'targetAttribute' => ['camiones_matricula' => 'matricula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'camiones_matricula' => 'Camiones Matricula',
            'camioneros_dni' => 'Camioneros Dni',
        ];
    }

    /**
     * Gets query for [[CamionerosDni]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionerosDni()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'camioneros_dni']);
    }

    /**
     * Gets query for [[CamionesMatricula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionesMatricula()
    {
        return $this->hasOne(Camiones::className(), ['matricula' => 'camiones_matricula']);
    }
    
    public function getCamiones()
    {
        $salida= Camiones::find()->all();
        return \yii\helpers\ArrayHelper::map($salida,"matricula","modelo");
    }
    
    public function getCamioneros()
    {
        $salida= Camioneros::find()->all();
        return \yii\helpers\ArrayHelper::map($salida,"dni",function($modelo){
            return $modelo->nombre . "--" . $modelo->poblacion;
        });
    }
}
