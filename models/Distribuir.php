<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribuir".
 *
 * @property string $camioneros_dni
 * @property string $paquetes_codigo
 *
 * @property Camioneros $camionerosDni
 * @property Paquetes $paquetesCodigo
 */
class Distribuir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribuir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['camioneros_dni', 'paquetes_codigo'], 'required'],
            [['camioneros_dni'], 'string', 'max' => 9],
            [['paquetes_codigo'], 'string', 'max' => 10],
            [['paquetes_codigo'], 'unique'],
            [['camioneros_dni', 'paquetes_codigo'], 'unique', 'targetAttribute' => ['camioneros_dni', 'paquetes_codigo']],
            [['camioneros_dni'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['camioneros_dni' => 'dni']],
            [['paquetes_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquetes_codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'camioneros_dni' => 'Camioneros Dni',
            'paquetes_codigo' => 'Paquetes Codigo',
        ];
    }

    /**
     * Gets query for [[CamionerosDni]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionerosDni()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'camioneros_dni']);
    }

    /**
     * Gets query for [[PaquetesCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesCodigo()
    {
        return $this->hasOne(Paquetes::className(), ['codigo' => 'paquetes_codigo']);
    }
    
    public function getCamioneros()
    {
        $salida= Camioneros::find()->all();
        return \yii\helpers\ArrayHelper::map($salida,"dni","nombre");
    }
    
    public function getPaquetes()
    {
        $salida= Paquetes::find()->all();
        return \yii\helpers\ArrayHelper::map($salida,"codigo",function($modelo){
            return $modelo->descripcion . "--" . $modelo->destinatario . "--" . $modelo->direccion_destinatario;
        });
    }
    
}
