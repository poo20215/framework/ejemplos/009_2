<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camiones".
 *
 * @property string $matricula
 * @property string|null $modelo
 * @property string|null $tipo
 * @property string|null $potencia
 *
 * @property Camioneros[] $camionerosDnis
 * @property Conducir[] $conducirs
 */
class Camiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'required'],
            [['matricula'], 'string', 'max' => 7],
            [['modelo', 'tipo', 'potencia'], 'string', 'max' => 50],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'matricula' => 'Matricula',
            'modelo' => 'Modelo',
            'tipo' => 'Tipo',
            'potencia' => 'Potencia',
        ];
    }

    /**
     * Gets query for [[CamionerosDnis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionerosDnis()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'camioneros_dni'])->viaTable('conducir', ['camiones_matricula' => 'matricula']);
    }

    /**
     * Gets query for [[Conducirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConducirs()
    {
        return $this->hasMany(Conducir::className(), ['camiones_matricula' => 'matricula']);
    }
}
