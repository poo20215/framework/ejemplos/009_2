<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property string $codigo
 * @property string|null $descripcion
 * @property string|null $destinatario
 * @property string|null $direccion_destinatario
 *
 * @property Camioneros[] $camionerosDnis
 * @property Distribuir $distribuir
 * @property Llegar $llegar
 * @property Provincias[] $provinciasCodigos
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'string', 'max' => 10],
            [['descripcion', 'destinatario'], 'string', 'max' => 70],
            [['direccion_destinatario'], 'string', 'max' => 100],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
            'destinatario' => 'Destinatario',
            'direccion_destinatario' => 'Direccion Destinatario',
        ];
    }

    /**
     * Gets query for [[CamionerosDnis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionerosDnis()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'camioneros_dni'])->viaTable('distribuir', ['paquetes_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Distribuir]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuir()
    {
        return $this->hasOne(Distribuir::className(), ['paquetes_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Llegar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlegar()
    {
        return $this->hasOne(Llegar::className(), ['paquetes_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[ProvinciasCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvinciasCodigos()
    {
        return $this->hasMany(Provincias::className(), ['codigo' => 'provincias_codigo'])->viaTable('llegar', ['paquetes_codigo' => 'codigo']);
    }
}
