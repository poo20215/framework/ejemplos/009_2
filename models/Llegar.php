<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "llegar".
 *
 * @property string $paquetes_codigo
 * @property string $provincias_codigo
 *
 * @property Paquetes $paquetesCodigo
 * @property Provincias $provinciasCodigo
 */
class Llegar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'llegar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paquetes_codigo', 'provincias_codigo'], 'required'],
            [['paquetes_codigo', 'provincias_codigo'], 'string', 'max' => 10],
            [['paquetes_codigo'], 'unique'],
            [['paquetes_codigo', 'provincias_codigo'], 'unique', 'targetAttribute' => ['paquetes_codigo', 'provincias_codigo']],
            [['paquetes_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquetes_codigo' => 'codigo']],
            [['provincias_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincias_codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paquetes_codigo' => 'Paquetes Codigo',
            'provincias_codigo' => 'Provincias Codigo',
        ];
    }

    /**
     * Gets query for [[PaquetesCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesCodigo()
    {
        return $this->hasOne(Paquetes::className(), ['codigo' => 'paquetes_codigo']);
    }

    /**
     * Gets query for [[ProvinciasCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvinciasCodigo()
    {
        return $this->hasOne(Provincias::className(), ['codigo' => 'provincias_codigo']);
    }
    
    public function getPaquetes()
    {
        $salida= Paquetes::find()->all();
        return \yii\helpers\ArrayHelper::map($salida,"codigo","descripcion");
    }
    
    public function getProvincias()
    {
        $salida= Provincias::find()->all();
        return \yii\helpers\ArrayHelper::map($salida,"codigo",function($modelo){
            return $modelo->codigo . "--" . $modelo->nombre;
        });
    }

}
