<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camioneros".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property float|null $salario
 * @property string|null $poblacion
 * @property string|null $telefono
 * @property string|null $direccion
 *
 * @property Camiones[] $camionesMatriculas
 * @property Conducir[] $conducirs
 * @property Distribuir[] $distribuirs
 * @property Paquetes[] $paquetesCodigos
 */
class Camioneros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camioneros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['salario'], 'number'],
            [['dni', 'telefono'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 60],
            [['poblacion', 'direccion'], 'string', 'max' => 100],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'salario' => 'Salario',
            'poblacion' => 'Poblacion',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
        ];
    }

    /**
     * Gets query for [[CamionesMatriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionesMatriculas()
    {
        return $this->hasMany(Camiones::className(), ['matricula' => 'camiones_matricula'])->viaTable('conducir', ['camioneros_dni' => 'dni']);
    }

    /**
     * Gets query for [[Conducirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConducirs()
    {
        return $this->hasMany(Conducir::className(), ['camioneros_dni' => 'dni']);
    }

    /**
     * Gets query for [[Distribuirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuirs()
    {
        return $this->hasMany(Distribuir::className(), ['camioneros_dni' => 'dni']);
    }

    /**
     * Gets query for [[PaquetesCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesCodigos()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'paquetes_codigo'])->viaTable('distribuir', ['camioneros_dni' => 'dni']);
    }
}
