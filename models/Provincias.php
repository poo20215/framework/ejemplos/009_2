<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property string $codigo
 * @property string|null $nombre
 *
 * @property Llegar[] $llegars
 * @property Paquetes[] $paquetesCodigos
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 30],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Llegars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlegars()
    {
        return $this->hasMany(Llegar::className(), ['provincias_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[PaquetesCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesCodigos()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'paquetes_codigo'])->viaTable('llegar', ['provincias_codigo' => 'codigo']);
    }
}
