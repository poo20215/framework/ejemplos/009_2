<?php

namespace app\controllers;

use app\models\Conducir;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConducirController implements the CRUD actions for Conducir model.
 */
class ConducirController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Conducir models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Conducir::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'camiones_matricula' => SORT_DESC,
                    'camioneros_dni' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conducir model.
     * @param string $camiones_matricula Camiones Matricula
     * @param string $camioneros_dni Camioneros Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($camiones_matricula, $camioneros_dni)
    {
        return $this->render('view', [
            'model' => $this->findModel($camiones_matricula, $camioneros_dni),
        ]);
    }

    /**
     * Creates a new Conducir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Conducir();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'camiones_matricula' => $model->camiones_matricula, 'camioneros_dni' => $model->camioneros_dni]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Conducir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $camiones_matricula Camiones Matricula
     * @param string $camioneros_dni Camioneros Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($camiones_matricula, $camioneros_dni)
    {
        $model = $this->findModel($camiones_matricula, $camioneros_dni);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'camiones_matricula' => $model->camiones_matricula, 'camioneros_dni' => $model->camioneros_dni]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Conducir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $camiones_matricula Camiones Matricula
     * @param string $camioneros_dni Camioneros Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($camiones_matricula, $camioneros_dni)
    {
        $this->findModel($camiones_matricula, $camioneros_dni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conducir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $camiones_matricula Camiones Matricula
     * @param string $camioneros_dni Camioneros Dni
     * @return Conducir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($camiones_matricula, $camioneros_dni)
    {
        if (($model = Conducir::findOne(['camiones_matricula' => $camiones_matricula, 'camioneros_dni' => $camioneros_dni])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
