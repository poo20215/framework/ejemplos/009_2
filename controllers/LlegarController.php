<?php

namespace app\controllers;

use app\models\Llegar;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LlegarController implements the CRUD actions for Llegar model.
 */
class LlegarController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Llegar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Llegar::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'paquetes_codigo' => SORT_DESC,
                    'provincias_codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Llegar model.
     * @param string $paquetes_codigo Paquetes Codigo
     * @param string $provincias_codigo Provincias Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($paquetes_codigo, $provincias_codigo)
    {
        return $this->render('view', [
            'model' => $this->findModel($paquetes_codigo, $provincias_codigo),
        ]);
    }

    /**
     * Creates a new Llegar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Llegar();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'paquetes_codigo' => $model->paquetes_codigo, 'provincias_codigo' => $model->provincias_codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Llegar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $paquetes_codigo Paquetes Codigo
     * @param string $provincias_codigo Provincias Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($paquetes_codigo, $provincias_codigo)
    {
        $model = $this->findModel($paquetes_codigo, $provincias_codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'paquetes_codigo' => $model->paquetes_codigo, 'provincias_codigo' => $model->provincias_codigo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Llegar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $paquetes_codigo Paquetes Codigo
     * @param string $provincias_codigo Provincias Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($paquetes_codigo, $provincias_codigo)
    {
        $this->findModel($paquetes_codigo, $provincias_codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Llegar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $paquetes_codigo Paquetes Codigo
     * @param string $provincias_codigo Provincias Codigo
     * @return Llegar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($paquetes_codigo, $provincias_codigo)
    {
        if (($model = Llegar::findOne(['paquetes_codigo' => $paquetes_codigo, 'provincias_codigo' => $provincias_codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
