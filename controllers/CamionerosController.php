<?php

namespace app\controllers;

use app\models\Camioneros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CamionerosController implements the CRUD actions for Camioneros model.
 */
class CamionerosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Camioneros models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Camioneros::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dni' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Camioneros model.
     * @param string $dni Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni),
        ]);
    }

    /**
     * Creates a new Camioneros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Camioneros();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dni' => $model->dni]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Camioneros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni)
    {
        $model = $this->findModel($dni);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni' => $model->dni]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Camioneros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni)
    {
        $this->findModel($dni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Camioneros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni Dni
     * @return Camioneros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni)
    {
        if (($model = Camioneros::findOne($dni)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
