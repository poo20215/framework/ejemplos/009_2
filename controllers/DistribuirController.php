<?php

namespace app\controllers;

use app\models\Distribuir;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DistribuirController implements the CRUD actions for Distribuir model.
 */
class DistribuirController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Distribuir models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Distribuir::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'camioneros_dni' => SORT_DESC,
                    'paquetes_codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distribuir model.
     * @param string $camioneros_dni Camioneros Dni
     * @param string $paquetes_codigo Paquetes Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($camioneros_dni, $paquetes_codigo)
    {
        return $this->render('view', [
            'model' => $this->findModel($camioneros_dni, $paquetes_codigo),
        ]);
    }

    /**
     * Creates a new Distribuir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Distribuir();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'camioneros_dni' => $model->camioneros_dni, 'paquetes_codigo' => $model->paquetes_codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Distribuir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $camioneros_dni Camioneros Dni
     * @param string $paquetes_codigo Paquetes Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($camioneros_dni, $paquetes_codigo)
    {
        $model = $this->findModel($camioneros_dni, $paquetes_codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'camioneros_dni' => $model->camioneros_dni, 'paquetes_codigo' => $model->paquetes_codigo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Distribuir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $camioneros_dni Camioneros Dni
     * @param string $paquetes_codigo Paquetes Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($camioneros_dni, $paquetes_codigo)
    {
        $this->findModel($camioneros_dni, $paquetes_codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Distribuir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $camioneros_dni Camioneros Dni
     * @param string $paquetes_codigo Paquetes Codigo
     * @return Distribuir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($camioneros_dni, $paquetes_codigo)
    {
        if (($model = Distribuir::findOne(['camioneros_dni' => $camioneros_dni, 'paquetes_codigo' => $paquetes_codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
