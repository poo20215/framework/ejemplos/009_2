<?php

namespace app\controllers;

use app\models\Camiones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CamionesController implements the CRUD actions for Camiones model.
 */
class CamionesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Camiones models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Camiones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'matricula' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Camiones model.
     * @param string $matricula Matricula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($matricula)
    {
        return $this->render('view', [
            'model' => $this->findModel($matricula),
        ]);
    }

    /**
     * Creates a new Camiones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Camiones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'matricula' => $model->matricula]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Camiones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $matricula Matricula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($matricula)
    {
        $model = $this->findModel($matricula);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'matricula' => $model->matricula]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Camiones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $matricula Matricula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($matricula)
    {
        $this->findModel($matricula)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Camiones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $matricula Matricula
     * @return Camiones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($matricula)
    {
        if (($model = Camiones::findOne($matricula)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
